import utils from "../node_modules/decentraland-ecs-utils/index"

////////////////////////////// COLORS //////////////////////////////

const black = new Material()
black.albedoColor = Color3.Black()
black.metallic = 1
black.roughness = 0.2

const green = new Material()
green.albedoColor = Color3.FromHexString("#ebebdc")

const greenblue = new Material()
greenblue.albedoColor = Color3.FromHexString("#B5B5B5")

const glass = new Material()
glass.transparencyMode = TransparencyMode.ALPHA_BLEND
glass.albedoColor = new Color4(0, 0, 0.3, 0.3)
glass.roughness = 0.03

const darkGlass = new Material()
darkGlass.transparencyMode = TransparencyMode.ALPHA_BLEND
darkGlass.albedoColor = new Color4(0, 0, 0.4, 0.9)
darkGlass.roughness = 0.08
darkGlass.emissiveIntensity = 6


////////////////////////////////////////// COLUMNS //////////////////////////////////////////

export class Column extends Entity{
  constructor(position:Vector3, scale:Vector3)
  {
      super()
      this.addComponent(new BoxShape())
      this.addComponent(new Transform({
          position: position, 
          scale: scale
      }))
      this.addComponent(green)
      engine.addEntity(this)
  }
}

const centerCenterColumn = new Column(new Vector3(8, 6, 8), new Vector3(0.5, 12, 0.5))
const centerRightColumn = new Column(new Vector3(8, 9.75, 4), new Vector3(0.5, 19.5, 0.5))
const centerRight2Column = new Column(new Vector3(11, 9.75, 4), new Vector3(0.5, 19.5, 0.5))
const centerBackColumn = new Column(new Vector3(15, 8.65, 8), new Vector3(0.5, 17.4, 0.5))

const frontCenterBottomColumn = new Column(new Vector3(1, 2, 8), new Vector3(0.5, 4, 0.5))
const frontCenterUpColumn = new Column(new Vector3(1, 17.75, 8), new Vector3(0.5, 3.9, 0.5))
const frontLeftBottomColumn = new Column(new Vector3(1, 4, 15), new Vector3(0.5, 8, 0.5))
const frontLeftUpColumn = new Column(new Vector3(1, 17.75, 15), new Vector3(0.5, 3.9, 0.5))
const frontRightBottomColumn = new Column(new Vector3(1, 4, 1), new Vector3(0.5, 8, 0.5))
const frontRightUpColumn = new Column(new Vector3(1, 17.75, 1), new Vector3(0.5, 3.9, 0.5))

const leftCenterColumn = new Column(new Vector3(8, 9.75, 15), new Vector3(0.5, 19.5, 0.5))
const leftBackColumn = new Column(new Vector3(15, 8.65, 15), new Vector3(0.5, 17.4, 0.5))

const rightCenterColumn = new Column(new Vector3(8, 9.75, 1), new Vector3(0.5, 19.5, 0.5))
const rightCenter2Column = new Column(new Vector3(11, 9.75, 1), new Vector3(0.5, 19.5, 0.5))
const rightBackColumn = new Column(new Vector3(15, 14.65, 1), new Vector3(0.5, 5.4, 0.5))


//////////////////////////////////////// PLATFORMS ////////////////////////////////////////

export class Platform extends Entity{
  constructor(position:Vector3, rotation: Quaternion, scale:Vector3)
  {
      super()
      this.addComponent(new BoxShape())
      this.addComponent(new Transform({
          position: position, 
          rotation: rotation,
          scale: scale
      }))
      this.addComponent(green)
      engine.addEntity(this)
  }
}

const floor1 = new Platform(new Vector3(8, 0, 8), new Quaternion(1,0,0,0), new Vector3(16, 0.5, 16))
const floor2 = new Platform(new Vector3(4, 4, 8), new Quaternion(1,0,0,0), new Vector3(8, 0.5, 16))
const floor3 = new Platform(new Vector3(4, 8, 8), new Quaternion(1,0,0,0), new Vector3(8, 0.5, 16))
const floor41 = new Platform(new Vector3(13.5, 12, 8), new Quaternion(1,0,0,0), new Vector3(5, 0.5, 16))
const floor42 = new Platform(new Vector3(9.5, 12, 10), new Quaternion(1,0,0,0), new Vector3(3, 0.5, 12))

const floor51 = new Platform(new Vector3(0.75, 16, 8), new Quaternion(1,0,0,0), new Vector3(1.5, 0.5, 16))
const floor52 = new Platform(new Vector3(5.75, 16, 8), new Quaternion(1,0,0,0), new Vector3(4.5, 0.5, 16))

const floor53 = new Platform(new Vector3(2.5, 16, 0.75), new Quaternion(1,0,0,0), new Vector3(2, 0.5, 1.5))
const floor54 = new Platform(new Vector3(2.5, 16, 11), new Quaternion(1,0,0,0), new Vector3(2, 0.5, 10))

const floor55 = new Platform(new Vector3(13.5, 16, 8), new Quaternion(1,0,0,0), new Vector3(5, 0.5, 16))
const floor56 = new Platform(new Vector3(9.5, 16, 10), new Quaternion(1,0,0,0), new Vector3(3, 0.5, 12))

const roof = new Platform(new Vector3(4.5, 19.5, 8), new Quaternion(1,0,0,0), new Vector3(9, 0.5, 16))
const roof2 = new Platform(new Vector3(10.5, 19.5, 2.5), new Quaternion(1,0,0,0), new Vector3(3, 0.5, 5))

////////////////////////////////////////////// FIRST FLOOR GLASS WALLS ////////////////////////////////////////

export class Glasswall1 extends Entity{
  constructor(position:Vector3, rotation: Quaternion, scale:Vector3)
  {
      super()
      this.addComponent(new BoxShape())
      this.addComponent(new Transform({
          position: position, 
          rotation: rotation,
          scale: scale
      }))
      this.addComponent(glass)
      engine.addEntity(this)
  }
}

const frontGlassWall1 = new Glasswall1(new Vector3(1, 2, 8), new Quaternion(0,0,0,0), new Vector3(0.05, 4, 14))
const leftGlassWall1 = new Glasswall1(new Vector3(4.5, 2, 15), new Quaternion(0,1,0,1), new Vector3(0.05, 4, 7))
const rightGlassWall1 = new Glasswall1(new Vector3(4.5, 2, 1), new Quaternion(0,1,0,1), new Vector3(0.05, 4, 7))

////////////////////////////////////////////// SECOND FLOOR GLASS WALLS ////////////////////////////////////////

export class Glasswall2 extends Entity{
  constructor(position:Vector3, rotation: Quaternion, scale:Vector3)
  {
      super()
      this.addComponent(new BoxShape())
      this.addComponent(new Transform({
          position: position, 
          rotation: rotation,
          scale: scale
      }))
      this.addComponent(glass)
      engine.addEntity(this)
  }
}

const frontGlassWall2 = new Glasswall2(new Vector3(1, 6, 8), new Quaternion(0,0,0,0), new Vector3(0.05, 4, 14))
const leftGlassWall2 = new Glasswall2(new Vector3(4.5, 6, 15), new Quaternion(0,1,0,1), new Vector3(0.05, 4, 7))
const rightGlassWall2 = new Glasswall2(new Vector3(4.5, 6, 1), new Quaternion(0,1,0,1), new Vector3(0.05, 4, 7))

////////////////////////////////////////////// THIRD AND FOURTH FLOOR GLASS WALLS ////////////////////////////////////////

export class Glasswall34 extends Entity{
  constructor(position:Vector3, rotation: Quaternion, scale:Vector3)
  {
      super()
      this.addComponent(new BoxShape())
      this.addComponent(new Transform({
          position: position, 
          rotation: rotation,
          scale: scale
      }))
      this.addComponent(glass)
      engine.addEntity(this)
  }
}

const frontGlassWall34 = new Glasswall34(new Vector3(1, 12, 8), new Quaternion(0,0,0,0), new Vector3(0.05, 8, 13.95))
const leftGlassWall34 = new Glasswall34(new Vector3(4.5, 12, 15), new Quaternion(0,1,0,1), new Vector3(0.05, 8, 6.95))
const rightGlassWall34 = new Glasswall34(new Vector3(4.5, 12, 1), new Quaternion(0,1,0,1), new Vector3(0.05, 8, 6.95))

const backGlassWall34 = new Glasswall34(new Vector3(15, 14, 4.5), new Quaternion(0,0,0,0), new Vector3(0.05, 4, 7))
const backrightGlassWall34 = new Glasswall34(new Vector3(13, 14, 1), new Quaternion(0,1,0,1), new Vector3(0.05, 4, 4))


////////////////////////////////////////////// FIFTH FLOOR GLASS WALLS ////////////////////////////////////////

export class Glasswall5 extends Entity{
  constructor(position:Vector3, rotation: Quaternion, scale:Vector3)
  {
      super()
      this.addComponent(new BoxShape())
      this.addComponent(new Transform({
          position: position, 
          rotation: rotation,
          scale: scale
      }))
      this.addComponent(glass)
      engine.addEntity(this)
  }
}

const frontGlassWall5 = new Glasswall5(new Vector3(1, 18, 8), new Quaternion(0,0,0,0), new Vector3(0.05, 4, 14))
const leftGlassWall5 = new Glasswall5(new Vector3(4.5, 18, 15), new Quaternion(0,1,0,1), new Vector3(0.05, 4, 7))
const rightGlassWall5 = new Glasswall5(new Vector3(4.5, 18, 1), new Quaternion(0,1,0,1), new Vector3(0.05, 4, 7))

const backBalcony = new Glasswall5(new Vector3(15, 16.65, 8), new Quaternion(0,0,0,0), new Vector3(0.05, 1.3, 14))
const rightBalcony = new Glasswall5(new Vector3(13, 16.65, 1), new Quaternion(0,1,0,1), new Vector3(0.05, 1.3, 4))
const leftBalcony = new Glasswall5(new Vector3(11.5, 16.65, 15), new Quaternion(0,1,0,1), new Vector3(0.05, 1.3, 7))

const balconyRightLamp = new Entity 
balconyRightLamp.addComponent(new SphereShape)
balconyRightLamp.addComponent(new Transform({
  position: new Vector3(15, 17.65, 1), 
  scale: new Vector3(0.3, 0.3, 0.3)
}))
balconyRightLamp.addComponent(green)
engine.addEntity(balconyRightLamp)

const balconyCenterLamp = new Entity 
balconyCenterLamp.addComponent(new SphereShape)
balconyCenterLamp.addComponent(new Transform({
  position: new Vector3(15, 17.65, 8), 
  scale: new Vector3(0.3, 0.3, 0.3)
}))
balconyCenterLamp.addComponent(green)
engine.addEntity(balconyCenterLamp)

const balconyLeftLamp = new Entity 
balconyLeftLamp.addComponent(new SphereShape)
balconyLeftLamp.addComponent(new Transform({
  position: new Vector3(15, 17.65, 15), 
  scale: new Vector3(0.3, 0.3, 0.3)
}))
balconyLeftLamp.addComponent(green)
engine.addEntity(balconyLeftLamp)

////////////////////////////////////////////// SOLID WALLS ///////////////////////////////////////////

export class Solidwall extends Entity{
  constructor(position:Vector3, rotation: Quaternion, scale:Vector3)
  {
      super()
      this.addComponent(new BoxShape())
      this.addComponent(new Transform({
          position: position, 
          rotation: rotation,
          scale: scale
      }))
      this.addComponent(greenblue)
      engine.addEntity(this)
  }
}

const leftSolidWall = new Solidwall(new Vector3(11.5, 8, 15), new Quaternion(0,1,0,1), new Vector3(0.1, 16, 6.9))
const backSolidWall = new Solidwall(new Vector3(15, 8, 11.5), new Quaternion(0,0,0,0), new Vector3(0.1, 16, 6.9))
const backRightSolidWalll = new Solidwall(new Vector3(11.5, 6, 8), new Quaternion(0,1,0,1), new Vector3(0.1, 12, 7))
const centerSolidWall = new Solidwall(new Vector3(8, 8, 6), new Quaternion(0,0,0,0), new Vector3(0.1, 8, 4))


////////////////////////////////////////////// LIFT /////////////////////////////////////////////////

////////////////////////////////////////////// LIFT WALLS

export class Liftwall extends Entity{
  constructor(position:Vector3, rotation: Quaternion, scale:Vector3)
  {
      super()
      this.addComponent(new BoxShape())
      this.addComponent(new Transform({
          position: position, 
          rotation: rotation,
          scale: scale
      }))
      this.addComponent(darkGlass)
      engine.addEntity(this)
  }
}

const rightLiftWall = new Liftwall(new Vector3(9.5, 9.75, 1), new Quaternion(0,1,0,1), new Vector3(0.05, 19.5, 3))
const backLiftWall = new Liftwall(new Vector3(11, 9.75, 2.5), new Quaternion(0,0,0,0), new Vector3(0.05, 19.5, 3))
const leftLiftWall1 = new Liftwall(new Vector3(9.5, 6.125, 4), new Quaternion(0,1,0,1), new Vector3(0.05, 12.25, 3))
const leftLiftWall2 = new Liftwall(new Vector3(9.5, 17.5, 4), new Quaternion(0,1,0,1), new Vector3(0.05, 3.5, 3))
const frontLiftWall = new Liftwall(new Vector3(8, 14, 2.5), new Quaternion(0,0,0,0), new Vector3(0.05, 4, 3))

////////////////////////////////////////////// LIFT

const firstFloor = new Vector3(9.5, 0.3, 2.5)
const secondFloor = new Vector3(9.5, 4.3, 2.5)
const thirdFloor = new Vector3(9.5, 8.3, 2.5)
const fourthFloor = new Vector3(9.5, 12.3, 2.5)
const fifthFloor = new Vector3(9.5, 16.3, 2.5)

const clip = new AudioClip('sounds/sound.mp3')
const sound = new AudioSource(clip)
sound.playing = false

const clip2 = new AudioClip('sounds/liftswitch.mp3')
const liftSwitch = new AudioSource(clip2)
liftSwitch.playing = false

const liftPlatform = new Entity 
liftPlatform.addComponent(new BoxShape)
liftPlatform.addComponent(new Transform({
  position: new Vector3(9.5, 0.3, 2.5),
  scale: new Vector3(2.5, 0.1, 2.5)
}))
const transformLiftPlatform = liftPlatform.getComponent(Transform)
liftPlatform.addComponent(darkGlass)
liftPlatform.addComponent(sound)
engine.addEntity(liftPlatform)

////////////////////////////////////////////// Lift getters

const liftGet1 = new Entity 
liftGet1.addComponent(new BoxShape)
liftGet1.addComponent(new Transform({
  position: new Vector3(7.7, 1.5, 4), 
  rotation: new Quaternion(1,0,1,0),
  scale: new Vector3(0.2, 0.2, 0.07)
}))
liftGet1.addComponent(darkGlass)
liftGet1.addComponent(
	new OnClick(() => {
    sound.playOnce()
    if (transformLiftPlatform.position.y == secondFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(secondFloor, firstFloor, 2, () => {
        transformLiftPlatform.position = firstFloor}))
    }
    else if (transformLiftPlatform.position.y == thirdFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(thirdFloor, firstFloor, 4, () => {
        transformLiftPlatform.position = firstFloor}))
    }
    else if (transformLiftPlatform.position.y == fourthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fourthFloor, firstFloor, 6, () => {
        transformLiftPlatform.position = firstFloor}))
    }
    else if (transformLiftPlatform.position.y == fifthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fifthFloor, firstFloor, 8, () => {
        transformLiftPlatform.position = firstFloor}))
    }
    else if (transformLiftPlatform.position.y == firstFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(firstFloor, firstFloor, 0, () => {
        transformLiftPlatform.position = firstFloor}))
    }
	})
)
engine.addEntity(liftGet1)

const liftGet2 = new Entity 
liftGet2.addComponent(new BoxShape)
liftGet2.addComponent(new Transform({
  position: new Vector3(7.7, 5.5, 4), 
  rotation: new Quaternion(1,0,1,0),
  scale: new Vector3(0.2, 0.2, 0.07)
}))
liftGet2.addComponent(darkGlass)
liftGet2.addComponent(
	new OnClick(() => {
    sound.playOnce()
    if (transformLiftPlatform.position.y == firstFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(firstFloor, secondFloor, 2, () => {
        transformLiftPlatform.position = secondFloor}))
    }
    else if (transformLiftPlatform.position.y == thirdFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(thirdFloor, secondFloor, 2, () => {
        transformLiftPlatform.position = secondFloor}))
    }
    else if (transformLiftPlatform.position.y == fourthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fourthFloor, secondFloor, 4, () => {
        transformLiftPlatform.position = secondFloor}))
    }
    else if (transformLiftPlatform.position.y == fifthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fifthFloor, secondFloor, 6, () => {
        transformLiftPlatform.position = secondFloor}))
    }
    else if (transformLiftPlatform.position.y == secondFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(secondFloor, secondFloor, 0, () => {
        transformLiftPlatform.position = secondFloor}))
    }
	})
)
engine.addEntity(liftGet2)

const liftGet3 = new Entity 
liftGet3.addComponent(new BoxShape)
liftGet3.addComponent(new Transform({
  position: new Vector3(7.7, 9.5, 4), 
  rotation: new Quaternion(1,0,1,0),
  scale: new Vector3(0.2, 0.2, 0.07)
}))
liftGet3.addComponent(darkGlass)
liftGet3.addComponent(
	new OnClick(() => {
    sound.playOnce()
    if (transformLiftPlatform.position.y == firstFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(firstFloor, thirdFloor, 4, () => {
        transformLiftPlatform.position = thirdFloor}))
    }
    else if (transformLiftPlatform.position.y == secondFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(secondFloor, thirdFloor, 2, () => {
        transformLiftPlatform.position = thirdFloor}))
    }
    else if (transformLiftPlatform.position.y == fourthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fourthFloor, thirdFloor, 2, () => {
        transformLiftPlatform.position = thirdFloor}))
    }
    else if (transformLiftPlatform.position.y == fifthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fifthFloor, thirdFloor, 4, () => {
        transformLiftPlatform.position = thirdFloor}))
    }
    else if (transformLiftPlatform.position.y == thirdFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(thirdFloor, thirdFloor, 0, () => {
        transformLiftPlatform.position = thirdFloor}))
    }
	})
)
engine.addEntity(liftGet3)

const liftGet4 = new Entity 
liftGet4.addComponent(new BoxShape)
liftGet4.addComponent(new Transform({
  position: new Vector3(11, 13.5, 4.3), 
  scale: new Vector3(0.2, 0.2, 0.07)
}))
liftGet4.addComponent(darkGlass)
liftGet4.addComponent(
	new OnClick(() => {
    sound.playOnce()
    if (transformLiftPlatform.position.y == firstFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(firstFloor, fourthFloor, 6, () => {
        transformLiftPlatform.position = fourthFloor}))
    }
    else if (transformLiftPlatform.position.y == secondFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(secondFloor, fourthFloor, 4, () => {
        transformLiftPlatform.position = fourthFloor}))
    }
    else if (transformLiftPlatform.position.y == thirdFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(thirdFloor, fourthFloor, 2, () => {
        transformLiftPlatform.position = fourthFloor}))
    }
    else if (transformLiftPlatform.position.y == fifthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fifthFloor, fourthFloor, 2, () => {
        transformLiftPlatform.position = fourthFloor}))
    }
    else if (transformLiftPlatform.position.y == fourthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fourthFloor, fourthFloor, 0, () => {
        transformLiftPlatform.position = fourthFloor}))
    }
	})
)
engine.addEntity(liftGet4)

const liftGet5 = new Entity 
liftGet5.addComponent(new BoxShape)
liftGet5.addComponent(new Transform({
  position: new Vector3(7.7, 17.5, 4), 
  rotation: new Quaternion(1,0,1,0),
  scale: new Vector3(0.2, 0.2, 0.07)
}))
liftGet5.addComponent(darkGlass)
liftGet5.addComponent(
	new OnClick(() => {
    sound.playOnce()
    if (transformLiftPlatform.position.y == firstFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(firstFloor, fifthFloor, 8, () => {
        transformLiftPlatform.position = fifthFloor}))
    }
    else if (transformLiftPlatform.position.y == secondFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(secondFloor, fifthFloor, 6, () => {
        transformLiftPlatform.position = fifthFloor}))
    }
    else if (transformLiftPlatform.position.y == thirdFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(thirdFloor, fifthFloor, 4, () => {
        transformLiftPlatform.position = fifthFloor}))
    }
    else if (transformLiftPlatform.position.y == fourthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fourthFloor, fifthFloor, 2, () => {
        transformLiftPlatform.position = fifthFloor}))
    }
    else if (transformLiftPlatform.position.y == fifthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fifthFloor, fifthFloor, 0, () => {
        transformLiftPlatform.position = fifthFloor}))
    }
	})
)
engine.addEntity(liftGet5)

////////////////////////////////////////////// Lift setters

const liftSet1 = new Entity 
liftSet1.setParent(liftPlatform)
liftSet1.addComponent(new SphereShape)
liftSet1.addComponent(new Transform({
  position: new Vector3(0.4, 12, -0.4), 
  scale: new Vector3(0.02, 0.5, 0.02)
}))
liftSet1.addComponent(darkGlass)
liftSet1.addComponent(liftSwitch)
liftSet1.addComponentOrReplace(
	new OnClick(() => {
    liftSwitch.playOnce()
    if (transformLiftPlatform.position.y == secondFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(secondFloor, firstFloor, 2, () => {
        transformLiftPlatform.position = firstFloor}))
    }
    else if (transformLiftPlatform.position.y == thirdFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(thirdFloor, firstFloor, 4, () => {
        transformLiftPlatform.position = firstFloor}))
    }
    else if (transformLiftPlatform.position.y == fourthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fourthFloor, firstFloor, 6, () => {
        transformLiftPlatform.position = firstFloor}))
    }
    else if (transformLiftPlatform.position.y == fifthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fifthFloor, firstFloor, 8, () => {
        transformLiftPlatform.position = firstFloor}))
    }
    else if (transformLiftPlatform.position.y == firstFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(firstFloor, firstFloor, 0, () => {
        transformLiftPlatform.position = firstFloor}))
    }
  })
)
engine.addEntity(liftSet1)

const liftSet2 = new Entity 
liftSet2.setParent(liftPlatform)
liftSet2.addComponent(new SphereShape)
liftSet2.addComponent(new Transform({
  position: new Vector3(0.4, 13, -0.4), 
  scale: new Vector3(0.02, 0.5, 0.02)
}))
liftSet2.addComponent(darkGlass)
liftSet2.addComponentOrReplace(
	new OnClick(() => {
    liftSwitch.playOnce()
    if (transformLiftPlatform.position.y == firstFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(firstFloor, secondFloor, 2, () => {
        transformLiftPlatform.position = secondFloor}))
    }
    else if (transformLiftPlatform.position.y == thirdFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(thirdFloor, secondFloor, 2, () => {
        transformLiftPlatform.position = secondFloor}))
    }
    else if (transformLiftPlatform.position.y == fourthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fourthFloor, secondFloor, 4, () => {
        transformLiftPlatform.position = secondFloor}))
    }
    else if (transformLiftPlatform.position.y == fifthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fifthFloor, secondFloor, 6, () => {
        transformLiftPlatform.position = secondFloor}))
    }
    else if (transformLiftPlatform.position.y == secondFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(secondFloor, secondFloor, 0, () => {
        transformLiftPlatform.position = secondFloor}))
    }
	})
)
engine.addEntity(liftSet2)

const liftSet3 = new Entity 
liftSet3.setParent(liftPlatform)
liftSet3.addComponent(new SphereShape)
liftSet3.addComponent(new Transform({
  position: new Vector3(0.4, 14, -0.4), 
  scale: new Vector3(0.02, 0.5, 0.02)
}))
liftSet3.addComponent(darkGlass)
liftSet3.addComponentOrReplace(
	new OnClick(() => {
    liftSwitch.playOnce()
    if (transformLiftPlatform.position.y == firstFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(firstFloor, thirdFloor, 4, () => {
        transformLiftPlatform.position = thirdFloor}))
    }
    else if (transformLiftPlatform.position.y == secondFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(secondFloor, thirdFloor, 2, () => {
        transformLiftPlatform.position = thirdFloor}))
    }
    else if (transformLiftPlatform.position.y == fourthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fourthFloor, thirdFloor, 2, () => {
        transformLiftPlatform.position = thirdFloor}))
    }
    else if (transformLiftPlatform.position.y == fifthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fifthFloor, thirdFloor, 4, () => {
        transformLiftPlatform.position = thirdFloor}))
    }
    else if (transformLiftPlatform.position.y == thirdFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(thirdFloor, thirdFloor, 0, () => {
        transformLiftPlatform.position = thirdFloor}))
    }
	})
)
engine.addEntity(liftSet3)

const liftSet4 = new Entity 
liftSet4.setParent(liftPlatform)
liftSet4.addComponent(new SphereShape)
liftSet4.addComponent(new Transform({
  position: new Vector3(0.4, 15, -0.4), 
  scale: new Vector3(0.02, 0.5, 0.02)
}))
liftSet4.addComponent(darkGlass)
liftSet4.addComponentOrReplace(
	new OnClick(() => {
    liftSwitch.playOnce()
    if (transformLiftPlatform.position.y == firstFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(firstFloor, fourthFloor, 6, () => {
        transformLiftPlatform.position = fourthFloor}))
    }
    else if (transformLiftPlatform.position.y == secondFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(secondFloor, fourthFloor, 4, () => {
        transformLiftPlatform.position = fourthFloor}))
    }
    else if (transformLiftPlatform.position.y == thirdFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(thirdFloor, fourthFloor, 2, () => {
        transformLiftPlatform.position = fourthFloor}))
    }
    else if (transformLiftPlatform.position.y == fifthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fifthFloor, fourthFloor, 2, () => {
        transformLiftPlatform.position = fourthFloor}))
    }
    else if (transformLiftPlatform.position.y == fourthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fourthFloor, fourthFloor, 0, () => {
        transformLiftPlatform.position = fourthFloor}))
    }
	})
)
engine.addEntity(liftSet4)

const liftSet5 = new Entity 
liftSet5.setParent(liftPlatform)
liftSet5.addComponent(new SphereShape)
liftSet5.addComponent(new Transform({
  position: new Vector3(0.4, 16, -0.4), 
  scale: new Vector3(0.02, 0.5, 0.02)
}))
liftSet5.addComponent(darkGlass)
liftSet5.addComponentOrReplace(
	new OnClick(() => {
    liftSwitch.playOnce()
    if (transformLiftPlatform.position.y == firstFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(firstFloor, fifthFloor, 8, () => {
        transformLiftPlatform.position = fifthFloor}))
    }
    else if (transformLiftPlatform.position.y == secondFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(secondFloor, fifthFloor, 6, () => {
        transformLiftPlatform.position = fifthFloor}))
    }
    else if (transformLiftPlatform.position.y == thirdFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(thirdFloor, fifthFloor, 4, () => {
        transformLiftPlatform.position = fifthFloor}))
    }
    else if (transformLiftPlatform.position.y == fourthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fourthFloor, fifthFloor, 2, () => {
        transformLiftPlatform.position = fifthFloor}))
    }
    else if (transformLiftPlatform.position.y == fifthFloor.y){
      liftPlatform.addComponentOrReplace(new utils.MoveTransformComponent(fifthFloor, fifthFloor, 0, () => {
        transformLiftPlatform.position = fifthFloor}))
    }
	})
)
engine.addEntity(liftSet5)


/////////////////////////////// STAITS ///////////////////////////////////////////

export class Step extends Entity{
  constructor(position:Vector3, rotation:Quaternion, scale:Vector3)
  {
      super()
      this.addComponent(new BoxShape())
      this.addComponent(new Transform({
          position: position, 
          rotation: rotation,
          scale: scale
      }))
      this.addComponent(darkGlass)
      engine.addEntity(this)
  }
}

/////////////////////////////// FIRST FLOOR STAITS /////////////////////////////// 

new Step(new Vector3(8.5, 0.53, 9.8), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(9, 0.81, 9.8), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(9.5, 1.09, 9.8), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(10, 1.37, 9.8), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(10.5, 1.65, 9.8), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(11, 1.93, 9.8), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(12.25, 2.21, 9.3), new Quaternion(0,0,0,0), new Vector3(2, 0.1, 2))
new Step(new Vector3(11, 2.49, 8.8), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(10.5, 2.77, 8.8), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(10, 3.05, 8.8), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(9.5, 3.33, 8.8), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(9, 3.61, 8.8), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(8.5, 3.89, 8.8), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))

/////////////////////////////// SECOND FLOOR STAITS /////////////////////////////// 

new Step(new Vector3(8.5, 4.53, 13.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(9, 4.81, 13.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(9.5, 5.09, 13.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(10, 5.37, 13.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(10.5, 5.65, 13.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(11, 5.93, 13.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(12.25, 6.21, 13.7), new Quaternion(0,0,0,0), new Vector3(2, 0.1, 2))
new Step(new Vector3(11, 6.49, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(10.5, 6.77, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(10, 7.05, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(9.5, 7.33, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(9, 7.61, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(8.5, 7.89, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))

/////////////////////////////// THIRD FLOOR STAITS /////////////////////////////// 

new Step(new Vector3(3, 8.36, 10.2), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(3, 8.62, 10.7), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(3, 8.88, 11.2), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(3, 9.14, 11.7), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(3, 9.40, 12.2), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2.5, 9.66, 13.7), new Quaternion(0,0,0,0), new Vector3(2, 0.1, 2))
new Step(new Vector3(3.75, 9.92, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(4.25, 10.18, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(4.75, 10.44, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(5.25, 10.7, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(5.75, 10.96, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(6.25, 11.22, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(6.75, 11.48, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(7.25, 11.74, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(7.75, 12.00, 14.2), new Quaternion(0,0,0,0), new Vector3(0.5, 0.1, 1))

new Step(new Vector3(2, 9.92, 12.2), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 10.18, 11.7), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 10.44, 11.2), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 10.7, 10.7), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 10.96, 10.2), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 11.22, 9.7), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 11.48, 9.2), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 11.74, 8.7), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 12.00, 8.2), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 12.26, 7.7), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 12.52, 7.2), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 12.78, 6.7), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 13.04, 6.2), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 13.3, 5.7), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 13.56, 5.2), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 13.82, 4.7), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 14.08, 4.2), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2, 14.34, 3.7), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(2.5, 14.6, 2.5), new Quaternion(0,0,0,0), new Vector3(2, 0.1, 2))

new Step(new Vector3(3, 14.96, 3.7), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(3, 15.22, 4.2), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(3, 15.48, 4.7), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(3, 15.74, 5.2), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))
new Step(new Vector3(3, 16, 5.7), new Quaternion(0,1,0,1), new Vector3(0.5, 0.1, 1))


///////////////////////////////////////// LIGHT SWITCH /////////////////////////////////////////
const clip3 = new AudioClip('sounds/switch.mp3')
const switch1 = new AudioSource(clip3)
switch1.playing = false

const Switch1 = new Entity 
Switch1.addComponent(new BoxShape)
Switch1.addComponent(new Transform({
  position: new Vector3(7.7, 1.5, 8), 
  rotation: new Quaternion(0,1,0,1),
  scale: new Vector3(0.07, 0.2, 0.07)
}))
let transform1 = Switch1.getComponent(Transform)
Switch1.addComponent(darkGlass)
Switch1.addComponent(switch1)
Switch1.addComponent(
	new OnClick(() => {
    switch1.playOnce()
    if (Switch1.hasComponent(darkGlass)){
      Switch1.addComponentOrReplace(glass),
      transform1.rotation = new Quaternion(1,0,0,1)
      frontGlassWall1.addComponentOrReplace(darkGlass)
      leftGlassWall1.addComponentOrReplace(darkGlass)
      rightGlassWall1.addComponentOrReplace(darkGlass)
    }
    else {
      Switch1.addComponentOrReplace(darkGlass)
      transform1.rotation = new Quaternion(0,1,0,1)
      frontGlassWall1.addComponentOrReplace(glass)
      leftGlassWall1.addComponentOrReplace(glass)
      rightGlassWall1.addComponentOrReplace(glass)
    }
	})
)
engine.addEntity(Switch1)

const switch2 = new AudioSource(clip3)
switch2.playing = false

const Switch2 = new Entity 
Switch2.addComponent(new BoxShape)
Switch2.addComponent(new Transform({
  position: new Vector3(7.7, 5.5, 8), 
  rotation: new Quaternion(0,1,0,1),
  scale: new Vector3(0.07, 0.2, 0.07)
}))
let transform2 = Switch2.getComponent(Transform)
Switch2.addComponent(darkGlass)
Switch2.addComponent(switch2)
Switch2.addComponent(
	new OnClick(() => {
    switch2.playOnce()
    if (Switch2.hasComponent(darkGlass)){
      Switch2.addComponentOrReplace(glass),
      transform2.rotation = new Quaternion(1,0,0,1)
      frontGlassWall2.addComponentOrReplace(darkGlass)
      leftGlassWall2.addComponentOrReplace(darkGlass)
      rightGlassWall2.addComponentOrReplace(darkGlass)
    }
    else {
      Switch2.addComponentOrReplace(darkGlass)
      transform2.rotation = new Quaternion(0,1,0,1)
      frontGlassWall2.addComponentOrReplace(glass)
      leftGlassWall2.addComponentOrReplace(glass)
      rightGlassWall2.addComponentOrReplace(glass)
    }
	})
)
engine.addEntity(Switch2)

const switch3 = new AudioSource(clip3)
switch3.playing = false

const Switch3 = new Entity 
Switch3.addComponent(new BoxShape)
Switch3.addComponent(new Transform({
  position: new Vector3(7.7, 9.5, 8), 
  rotation: new Quaternion(0,1,0,1),
  scale: new Vector3(0.07, 0.2, 0.07)
}))
let transform3 = Switch3.getComponent(Transform)
Switch3.addComponent(darkGlass)
Switch3.addComponent(switch3)
Switch3.addComponent(
	new OnClick(() => {
    switch3.playOnce()
    if (Switch3.hasComponent(darkGlass)){
      Switch3.addComponentOrReplace(glass),
      transform3.rotation = new Quaternion(1,0,0,1)
      frontGlassWall34.addComponentOrReplace(darkGlass)
      leftGlassWall34.addComponentOrReplace(darkGlass)
      rightGlassWall34.addComponentOrReplace(darkGlass)
    }
    else {
      Switch3.addComponentOrReplace(darkGlass)
      transform3.rotation = new Quaternion(0,1,0,1)
      frontGlassWall34.addComponentOrReplace(glass)
      leftGlassWall34.addComponentOrReplace(glass)
      rightGlassWall34.addComponentOrReplace(glass)
    }
	})
)
engine.addEntity(Switch3)

const switch4 = new AudioSource(clip3)
switch4.playing = false

const Switch4 = new Entity 
Switch4.addComponent(new BoxShape)
Switch4.addComponent(new Transform({
  position: new Vector3(14.7, 13.5, 8), 
  rotation: new Quaternion(0,1,0,1),
  scale: new Vector3(0.07, 0.2, 0.07)
}))
let transform4 = Switch4.getComponent(Transform)
Switch4.addComponent(darkGlass)
Switch4.addComponent(switch4)
Switch4.addComponent(
	new OnClick(() => {
    switch4.playOnce()
    if (Switch4.hasComponent(darkGlass)){
      Switch4.addComponentOrReplace(glass),
      transform4.rotation = new Quaternion(1,0,0,1)
      backGlassWall34.addComponentOrReplace(darkGlass)
      backrightGlassWall34.addComponentOrReplace(darkGlass)
    }
    else {
      Switch4.addComponentOrReplace(darkGlass)
      transform4.rotation = new Quaternion(0,1,0,1)
      backGlassWall34.addComponentOrReplace(glass)
      backrightGlassWall34.addComponentOrReplace(glass)
    }
	})
)
engine.addEntity(Switch4)

const switch5 = new AudioSource(clip3)
switch5.playing = false

const Switch5 = new Entity 
Switch5.addComponent(new BoxShape)
Switch5.addComponent(new Transform({
  position: new Vector3(1.3, 17.5, 8), 
  rotation: new Quaternion(0,1,0,1),
  scale: new Vector3(0.07, 0.2, 0.07)
}))
let transform5 = Switch5.getComponent(Transform)
Switch5.addComponent(darkGlass)
Switch5.addComponent(switch5)
Switch5.addComponent(
	new OnClick(() => {
    switch5.playOnce()
    if (Switch5.hasComponent(darkGlass)){
      Switch5.addComponentOrReplace(glass),
      transform5.rotation = new Quaternion(1,0,0,1)
      frontGlassWall5.addComponentOrReplace(darkGlass)
      leftGlassWall5.addComponentOrReplace(darkGlass)
      rightGlassWall5.addComponentOrReplace(darkGlass)
      backBalcony.addComponentOrReplace(darkGlass)
      rightBalcony.addComponentOrReplace(darkGlass)
      leftBalcony.addComponentOrReplace(darkGlass)
    }
    else {
      Switch5.addComponentOrReplace(darkGlass)
      transform5.rotation = new Quaternion(0,1,0,1)
      frontGlassWall5.addComponentOrReplace(glass)
      leftGlassWall5.addComponentOrReplace(glass)
      rightGlassWall5.addComponentOrReplace(glass)
      backBalcony.addComponentOrReplace(glass)
      rightBalcony.addComponentOrReplace(glass)
      leftBalcony.addComponentOrReplace(glass)
    }
	})
)
engine.addEntity(Switch5)

///////////////////////////////////////// DECOR /////////////////////////////////////////

///////////////////////////////////////// PIC 1 /////////////////////////////////////////

const metropolisTexture = new Texture("models/metropolis.png")
const metropolis = new Material()
metropolis.albedoTexture = metropolisTexture

const picture1 = new Entity()
picture1.addComponent(new BoxShape())
picture1.addComponent(new Transform({
  position: new Vector3(8.1, 7.65, 6), 
  rotation: Quaternion.Euler(90, 0, 0),
  scale: new Vector3(0.1, 2.8, 7)
}))
picture1.addComponent(metropolis)
engine.addEntity(picture1)

const picture1Frame1 = new Entity 
picture1Frame1.addComponent(new BoxShape)
picture1Frame1.addComponent(new Transform({
  position: new Vector3(8.1, 4.2, 6), 
  rotation: Quaternion.Euler(90, 0, 0),
  scale: new Vector3(0.15, 3.3, 0.15)
}))
picture1Frame1.addComponent(black)
engine.addEntity(picture1Frame1)

const picture1Frame2 = new Entity 
picture1Frame2.addComponent(new BoxShape)
picture1Frame2.addComponent(new Transform({
  position: new Vector3(8.1, 11.2, 6), 
  rotation: Quaternion.Euler(90, 0, 0),
  scale: new Vector3(0.15, 3.3, 0.15)
}))
picture1Frame2.addComponent(black)
engine.addEntity(picture1Frame2)

///////////////////////////////////////// PIC 2 /////////////////////////////////////////

const bladerunnerTexture = new Texture("models/bladerunner.jpg")
const bladerunner = new Material()
bladerunner.albedoTexture = bladerunnerTexture

const clip4 = new AudioClip('sounds/daleko.mp3')
const music = new AudioSource(clip4)
music.playing = false

const picture2 = new Entity()
picture2.addComponent(new BoxShape())
picture2.addComponent(new Transform({
  position: new Vector3(14.9, 6, 11.5), 
  rotation: Quaternion.Euler(270, 0, 0),
  scale: new Vector3(0.1, 5, 10)
}))
picture2.addComponent(bladerunner)
picture2.addComponent(music)
picture2.addComponent(
	new OnClick(() => {
    music.playOnce()
	})
)

engine.addEntity(picture2)

const picture2Frame1 = new Entity 
picture2Frame1.addComponent(new BoxShape)
picture2Frame1.addComponent(new Transform({
  position: new Vector3(14.9, 1, 11.5), 
  rotation: Quaternion.Euler(90, 0, 0),
  scale: new Vector3(0.15, 5.3, 0.15)
}))
picture2Frame1.addComponent(black)
engine.addEntity(picture2Frame1)

const picture2Frame2 = new Entity 
picture2Frame2.addComponent(new BoxShape)
picture2Frame2.addComponent(new Transform({
  position: new Vector3(14.9, 11, 11.5), 
  rotation: Quaternion.Euler(90, 0, 0),
  scale: new Vector3(0.15, 5.3, 0.15)
}))
picture2Frame2.addComponent(black)
engine.addEntity(picture2Frame2)

const picture2Frame3 = new Entity 
picture2Frame3.addComponent(new BoxShape)
picture2Frame3.addComponent(new Transform({
  position: new Vector3(14.9, 6, 14), 
  rotation: Quaternion.Euler(0, 0, 0),
  scale: new Vector3(0.15, 10.3, 0.15)
}))
picture2Frame3.addComponent(black)
engine.addEntity(picture2Frame3)

const picture2Frame4 = new Entity 
picture2Frame4.addComponent(new BoxShape)
picture2Frame4.addComponent(new Transform({
  position: new Vector3(14.9, 6, 9), 
  rotation: Quaternion.Euler(0, 0, 0),
  scale: new Vector3(0.15, 10.3, 0.15)
}))
picture2Frame4.addComponent(black)
engine.addEntity(picture2Frame4)